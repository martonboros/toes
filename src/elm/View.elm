module View exposing (view)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)
import List exposing (head, filter, indexedMap)
import TicTacToe exposing (Player(..), Position(..), Game, GameResult(..), ifX, getMoves, result, whoseMove)
import Model exposing (Leaderboard, Model)
import Update exposing (Msg(..), validPlayers)
import MyStyle exposing (..)
import Dict


view : Model -> Html Msg
view ({ mGame } as model) =
    div mainAttrs
        [ case mGame of
            Just game ->
                viewGame model game

            _ ->
                viewMenu model
        ]


viewHeader : Model -> String -> String -> Html Msg
viewHeader model titleText buttonText =
    let
        btn =
            button
                (buttonAttrs model.size
                    ++ [ disabled (not (validPlayers model)), onClick NewGame ]
                )
                [ text buttonText ]
    in
        ifWide model.size
            (div []
                [ h3 [] [ text titleText ]
                , p [] [ btn ]
                ]
            )
            (h3 []
                [ text titleText
                , btn
                ]
            )


viewMenu : Model -> Html Msg
viewMenu ({ playerX, playerO, leaderboard } as model) =
    viewLayout model
        [ viewHeader model "Toes!" "Play"
        , div [ class "row" ]
            [ viewPlayerInput model playerX ChangePlayerX X
            , viewPlayerInput model playerO ChangePlayerO O
            ]
        ]
        [ viewLeaderboard model ]


viewLayout : Model -> List (Html msg) -> List (Html msg) -> Html msg
viewLayout model content1 content2 =
    div containerAttrs <|
        ifWide model.size
            [ div [ class "row" ]
                [ div [ class "col s5 l3" ] content1
                , div [ class "col s6 l8" ] content2
                ]
            ]
            (content1 ++ content2)


viewLeaderboard : Model -> Html Msg
viewLeaderboard ({ leaderboard } as model) =
    if Dict.isEmpty leaderboard then
        text ""
    else
        table (tableAttrs model.size)
            [ thead []
                [ tr []
                    [ th [] [ text "Name" ]
                    , th [] [ text "Wins" ]
                    , th [] [ text "Losses" ]
                    , th [] [ text "Draws" ]
                    ]
                ]
            , tbody []
                (leaderboard
                    |> Dict.toList
                    |> List.sortBy (\( _, ( wins, _, _ ) ) -> 0 - wins)
                    |> List.map
                        (\( name, ( w, l, b ) ) ->
                            tr []
                                [ td [] [ text name ]
                                , td [] [ text (toString w) ]
                                , td [] [ text (toString l) ]
                                , td [] [ text (toString b) ]
                                ]
                        )
                )
            ]


viewPlayerBox : Model -> Player -> Bool -> Bool -> List (Html Msg) -> String -> Html Msg
viewPlayerBox model player myTurn iWin content score =
    div (playerBoxAttrs model.size)
        [ div
            (cardAttrs myTurn iWin)
            [ div playerBoxCardAttrs
                [ div playerBoxEdgeAttrs [ text (strPlayer player ++ " ") ]
                , div playerBoxContentAttrs content
                , div playerBoxEdgeAttrs [ text score ]
                ]
            ]
        ]


viewPlayerInput : Model -> String -> (String -> Msg) -> Player -> Html Msg
viewPlayerInput model name msg player =
    (viewPlayerBox model player False False)
        [ input
            (playerInputAttrs
                ++ [ placeholder (strPlayer player ++ " player's name")
                   , value name
                   , onInput msg
                   ]
            )
            []
        ]
        ""


viewGame : Model -> Game -> Html Msg
viewGame ({ playerX, playerO, leaderboard, size } as model) game =
    let
        res =
            result game

        whoseTurn =
            game |> getMoves |> List.length |> whoseMove

        playerName player =
            ifX player playerX playerO

        getScore name =
            leaderboard
                |> Dict.get name
                |> Maybe.map (\( w, l, d ) -> w)
                |> Maybe.withDefault 0

        ( status, mWinner, over ) =
            case res of
                Win winner _ ->
                    ( strPlayer winner ++ " wins!", Just winner, True )

                Draw ->
                    ( "Draw", Nothing, True )

                InProgress ->
                    ( strPlayer whoseTurn ++ "'s turn", Nothing, False )

        playerBox player name =
            (viewPlayerBox model player)
                (whoseTurn == player && not over)
                (mWinner == Just player)
                [ div playerNameAttrs [ text name ] ]
                (toString (getScore name))

        playerBoxes =
            div playerBoxesAttrs
                [ playerBox X playerX
                , playerBox O playerO
                ]

        squareSizeTry =
            ifWide model.size
                (toFloat size.height / 4)
                (toFloat size.height / 6)

        squareSize =
            if squareSizeTry > 220 then
                220
            else
                squareSizeTry

        v =
            viewPosition squareSize game res

        board =
            div (boardAttrs model.size squareSize)
                [ div boardRowAttrs [ v TL, v TC, v TR ]
                , div boardRowAttrs [ v CL, v CC, v CR ]
                , div boardRowAttrs [ v BL, v BC, v BR ]
                ]
    in
        viewLayout model
            [ viewHeader model status "New Game"
            , playerBoxes
            ]
            [ board ]


viewPosition : Float -> Game -> GameResult -> Position -> Html Msg
viewPosition size game res place =
    let
        occupier =
            game
                |> getMoves
                |> filter (\( _, p ) -> p == place)
                |> List.map Tuple.first
                |> head

        winning =
            case res of
                Win _ combos ->
                    List.member place (List.concat combos)

                _ ->
                    False
    in
        div
            (positionAttrs place occupier winning size ++ [ onClick (Move place) ])
            [ text (strOccupier occupier) ]


strOccupier : Maybe Player -> String
strOccupier mPlayer =
    mPlayer |> Maybe.map strPlayer |> Maybe.withDefault ""


strPlayer : Player -> String
strPlayer player =
    ifX player "×" "●"
