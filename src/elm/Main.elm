module Main exposing (..)

import Html exposing (beginnerProgram)
import Model exposing (Model, init)
import Update exposing (Msg(..), update)
import View exposing (view)
import Window exposing (size, resizes)
import Task exposing (perform)


main : Program Never Model Msg
main =
    Html.program
        { init = ( init, size |> perform Resize )
        , view = view
        , update = \msg model -> ( update msg model, Cmd.none )
        , subscriptions = \_ -> resizes Resize
        }
