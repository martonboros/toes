module Update exposing (Msg(..), update, validPlayers)

import Model exposing (Leaderboard, Model)
import TicTacToe exposing (Player(..), Position, Game, GameResult(..), ifX, move, empty, result)
import Dict
import Window exposing (Size)


type Msg
    = ChangePlayerX String
    | ChangePlayerO String
    | NewGame
    | Move Position
    | Resize Size


validPlayer : String -> Bool
validPlayer s =
    String.length s > 0


validPlayers : Model -> Bool
validPlayers { playerX, playerO } =
    validPlayer playerX && validPlayer playerO


updateLeaderboard : String -> String -> Game -> Leaderboard -> Leaderboard
updateLeaderboard playerX playerO game leaderboard =
    let
        updatePlayer player ( w, l, d ) =
            Dict.update player
                (\entry ->
                    case entry of
                        Just ( wins, losses, draws ) ->
                            Just ( wins + w, losses + l, draws + d )

                        Nothing ->
                            Just ( w, l, d )
                )
    in
        case result game of
            Win player _ ->
                let
                    ( winner, loser ) =
                        ifX player ( playerX, playerO ) ( playerO, playerX )
                in
                    leaderboard
                        |> updatePlayer winner ( 1, 0, 0 )
                        |> updatePlayer loser ( 0, 1, 0 )

            Draw ->
                leaderboard
                    |> updatePlayer playerX ( 0, 0, 1 )
                    |> updatePlayer playerO ( 0, 0, 1 )

            InProgress ->
                leaderboard


update : Msg -> Model -> Model
update msg ({ playerX, playerO, mGame, leaderboard } as model) =
    case msg of
        ChangePlayerX s ->
            { model | playerX = s }

        ChangePlayerO s ->
            { model | playerO = s }

        NewGame ->
            case mGame of
                Just game ->
                    { model | playerX = playerO, playerO = playerX, mGame = Nothing }

                Nothing ->
                    if validPlayers model then
                        { model | mGame = Just empty }
                    else
                        model

        Move place ->
            mGame
                |> Maybe.andThen (move place)
                |> Maybe.map
                    (\game ->
                        { model
                            | mGame = Just game
                            , leaderboard = updateLeaderboard playerX playerO game leaderboard
                        }
                    )
                |> Maybe.withDefault model

        Resize size ->
            { model | size = size }
