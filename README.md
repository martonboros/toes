# toes

a tic-tac-toe game

# [try it](https://martonboros.gitlab.io/toes/)

### workflow

issue → branch → (auto) test → merge request → merge → master → (auto) deploy


### develop, test, build

[get nix](https://nixos.org/nix/download.html), run `nix-shell` to get a shell with all dependencies available, then run

```bash
# start dev server on http://localhost:8080
npm start

npm run test

npm run build
```

### notes

- full CI/CD - pushed code is automatically tested, merges to master are automatically deployed
- the workflow above is enforced by repository settings; pushing to master is not allowed, MRs can only be merged if tests pass and fast-forward merge would be possible. take a look at the (closed) [issues](https://gitlab.com/martonboros/toes/issues?scope=all&utf8=%E2%9C%93&state=all), [merge requests](https://gitlab.com/brilliant-data/qnject-tde/merge_requests?scope=all&utf8=%E2%9C%93&state=all), and [network graph](https://gitlab.com/martonboros/toes/network/master)
- the [nix](https://nixos.org/nix/) purely functional package manager is used for hash-based dependencies (like yarn but for any kind of package), although npm is still used for node deps

### wishlist

- get rid of css class hacks
- snapshot testing
