module Model exposing (Leaderboard, Model, init)

import Dict exposing (Dict)
import TicTacToe exposing (Game)
import Window exposing (Size)


type alias Leaderboard =
    Dict String ( Int, Int, Int )


type alias Model =
    { playerX : String
    , playerO : String
    , mGame : Maybe Game
    , leaderboard : Leaderboard
    , size : Size
    }


init : Model
init =
    { playerX = "Player One"
    , playerO = "Player Zero"
    , mGame = Nothing
    , leaderboard = Dict.empty
    , size = { width = 0, height = 0 }
    }
